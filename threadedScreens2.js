// Threaded screenshots

// Using EXEC

var count = 0;
var threadMax = 10;

var exec = require('child_process').exec,
    child;


function getScreenshot(url){

	child = exec("open -a /Applications/Firefox.app/Contents/MacOS/firefox http://www.kaigani.com/ --args -saveimage -captureflash -width 1280 -height 960 -saveoptions delay=2000,flashdelay=2000",
	  function (error, stdout, stderr) {
	    console.log('stdout: ' + stdout);
	    console.log('stderr: ' + stderr);
	    if (error !== null) {
	      console.log('exec error: ' + error);
	    }
	  });
}

getScreenshot("http://www.google.com");

console.log("Count at main thread exit:" + count);

