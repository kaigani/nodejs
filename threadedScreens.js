// Threaded screenshots

// Using EXEC

var count = 0;
var threadMax = 10;

var exec = require('child_process').exec,
    child;

function startThread(){
	count++;
	console.log("Incremented count to: " + count);
}

function endThread(){
	count--;
	console.log("Decremented count to: "+ count);
}

function getScreenshot(url){
	startThread();
	child = exec("open -a /Applications/Firefox.app/Contents/MacOS/firefox '"+url+"' --args -saveimage -captureflash -width 1280 -height 960 -saveoptions delay=2000,flashdelay=2000",
	  function (error, stdout, stderr) {
	    console.log('stdout: ' + stdout);
	    console.log('stderr: ' + stderr);
	    if (error !== null) {
	      console.log('exec error: ' + error);
	    }
	    endThread();
	  });
}

getScreenshot("http://www.google.com");
/*
for(var i=0; i<1; i++){
	getScreenshot("http://www.kaigani.com");
	console.log("Completed loop i="+i);
}
*/
console.log("Count at main thread exit:" + count);

