fs = require('fs');

// Part 1 - READ
fs.readFile('_userDB/_whitelist_0-full.json', 'utf8', function (err,data) {
  if (err) {
	console.log('Error reading whitelist');
    console.log(err.stack);
    return console.log(err);
  }
  console.log("Read whitelist");
  var whitelist = eval(data);
  for(var i=0; i<whitelist.length; i++){
	console.log('Found '+whitelist[i].screen_name);
	newEntry(whitelist[i].screen_name,whitelist[i].since_id);
  }
});

// Part 2 - WRITE

function newEntry(screen_name, since_id){

	var o = {};
	o.screen_name = screen_name;
	o.since_id = since_id;
	var output = {};
	output.data = o;

	var jsonOutput = JSON.stringify(output);

	fs.writeFileSync('_userDB/sn_'+screen_name+'.json',jsonOutput,'utf8');
/*
	fs.writeFile('_userDB/sn_'+screen_name+'.json',jsonOutput,'utf8',function (err){
		if (err) {
			console.log("FAILED TO WRITE: "+screen_name);
			console.log(err.stack);
			return console.log(err);
		}
		console.log("Wrote: "+screen_name);
	});
*/
}

