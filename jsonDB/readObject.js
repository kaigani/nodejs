fs = require('fs');

// Part 1 - READ
fs.readFile('_output/output.json', 'utf8', function (err,data) {
  if (err) {
    console.log(err.stack);
    return console.log(err);
  }
  //console.log(data);
  var myObject = JSON.parse(data);
  //console.log("\n"+myObject.id_str);
  console.log("Read output.json");
});

// Part 2 - WRITE
var o = {};
o.list = [];
o.list.push('kai');
o.list.push('turner');
o.id = 12345;
o.name = "kaigani";
o.isNice = true;
o.object = { 'foo' : 'bar', 'johnny' : 'appleseed'};

var jsonOutput = JSON.stringify(o);

fs.writeFile('_output/noutput.json',jsonOutput,'utf8',function (err){
  if (err) {
	console.log("FAILED TO WRITE");
    console.log(err.stack);
    return console.log(err);
  }
  console.log("Wrote noutput.json");
});

// Part 3 - READ DIRECTORY & LOAD THE 'DATABASE'

var db = [];
fs.readdir('_output', function(err,files){
	if(err) {
		console.log("FAILED TO READ DIRECTORY");
		console.log(err.stack);
		return console.log(err);
	}
	for(var i=0; i<files.length;i++){
		var file = files[i];
		// SHOULD USE A UNIQUE PREFIX
		console.log(file);
		if(!file.match(/^\./)){
			var filename = '_output/'+files[i];
			var data = fs.readFileSync(filename,'utf8');
			console.log("READ "+filename);
			var value = JSON.parse(data);
			var key = file.replace(".json",'');
			var entry = {};
			entry._id = key;
			entry.data = value;
			db.push(entry);
		}
	}
	console.log("\nRead the database\n----------");
	console.log(JSON.stringify(db));
	writeDB(db);
});

// Part 4 - WRITE the files back out
function writeDB(database){

	// Add something to the DB
	var myNewObj = {};
	myNewObj.name = "FoShizzle";
	myNewObj.title = "MyNizzle";
	var newEntry = {};
	newEntry._id = "BrandNew";
	newEntry.data = myNewObj;
	database.push(newEntry);

	// Write it
	console.log('Writing the DB back out');
	for(var i=0; i<database.length; i++){
		var file = database[i]._id+".json";
		var data = JSON.stringify(database[i].data);
		var filename = "_output/"+file;
		fs.writeFileSync(filename,data,'utf8');
		console.log("Wrote "+filename);
	}
}
