var https = require('https');
var querystring = require('querystring');

var data = querystring.stringify({
      data: ['please','hammer','dont','hurt','em']
    });

var options = {
    host: 'script.google.com',
    port: 443,
    path: '/macros/s/AKfycbxwrSFKEDThQafeDz9ydodwgITzJ35FfL16eijVRpZLnJSXX7m5/exec',
    method: 'POST',
    rejectUnauthorized: false,
    headers: {
        'Content-Type': 'application/json',
        'Content-Length': data.length
    }
};

var req = https.request(options, function(res) {
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
        console.log("body: " + chunk);
    });
});

req.write(data);
req.end();