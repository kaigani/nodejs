
// TEST WRITE BACK
console.log("ATTEMPT JSON WRITEBACK");

var json = JSON.stringify(['this','is','my','time']);
var https = require('https');
var options = {
  host: 'script.google.com',
  port: 443,
  path: '/macros/s/AKfycbxwrSFKEDThQafeDz9ydodwgITzJ35FfL16eijVRpZLnJSXX7m5/exec?data='+json,
  headers: {
        'Content-Type': 'application/json'
    }
};

https.get(options, function(res) {
  console.log('STATUS: ' + res.statusCode);
  console.log('HEADERS: ' + JSON.stringify(res.headers));
  res.on('data', function(data) {
    console.log('DATA: ' + data);
    }).on('end', function(data) {
    console.log('END DATA: '+ data);
    });
}).on('error', function(e) {
  console.log('ERROR: ' + e.message);
});
