var sys = require('sys');
var wwwdude = require('wwwdude');

var client = wwwdude.createClient({
    headers: { 'User-Agent': 'wwwdude test 42' },
    gzip: true,
    timeout: 500 // 500ms timeout
  });

client.get('http://google.com/')
  .addListener('error', function (err) {
      sys.puts('Network Error: ' + sys.inspect(err));
    })
  .addListener('http-error', function (data, resp) {
      sys.puts('HTTP Error for: ' + resp.host + ' code: ' + resp.statusCode);
    })
  .addListener('redirect', function (data, resp) {
      sys.puts('Redirecting to: ' + resp.headers['location']);
      sys.puts('Headers: ' + sys.inspect(resp.headers));
    })
  .addListener('success', function (data, resp) {
      sys.debug('Got data: ' + data);
      sys.puts('Headers: ' + sys.inspect(resp.headers));
    });