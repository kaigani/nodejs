var terminal = require('child_process').spawn('bash');

terminal.stdout.on('data', function (data) {
    console.log('stdout: ' + data );
});

terminal.stderr.on('data', function (data) {
    console.log('stderr: ' + data );
});


terminal.on('exit', function (code) {
    console.log('child process exited with code ' + code);
});

setTimeout(function() {
    console.log('Sending stdin to terminal');
    terminal.stdin.write('echo "Hello $USER. Your machine runs since:"\n');
    terminal.stdin.write('uptime\n');
    terminal.stdin.write('ls\n');
    terminal.stdin.write('git status\n');
    console.log('Ending terminal session');
    terminal.stdin.end();
}, 1000);