// batchFile
// ---------
// Takes a file as input, regex find, regex replace and a file as output (optional)
//

console.log("REMEMBER TO ESCAPE ANY '$' CHARACTERS");

var fs = require('fs');

var args = process.argv.slice(2); // [ file1, regex, (file2) ]

// Read arguments 
var inputFile = args[0];
var regexFind = args[1];
var regexReplace = args[2];
var outputFile = (args[3])?args[3]:"OUTPUT.txt";

fs.readFile(inputFile, 'utf8', function (err, data) {
  if (err) throw err;
  console.log('Retrieved: '+inputFile);
  processFile(data);
});

function processFile(data){

	var evalString = 'data.replace('+regexFind+','+regexReplace+');';

	console.log('Processing: '+evalString);

	var newData = eval(evalString);

	fs.writeFile(outputFile, newData, function(err) {
		if (err) throw err;
		console.log('file saved: '+outputFile);
	});
}
