var http = require('http'),
	https = require('https');

var fetchUrl = {};

fetchUrl.get = function(url,callback,depth){
	
	var parent = this;
	depth = (depth)?depth:0;

	var body = [];

	var getFunction = (url.match(/^https/i))?https.get:http.get;

	getFunction(url, function(res) {
		console.log("statusCode: ", res.statusCode);

		//console.log("headers: ", res.headers);

		// handle 404 errors and others
		// TODO 301 & 302 redirects!
		if(res.statusCode === 301 || res.statusCode === 302){
			console.log("\t ++ New location:", res.headers.location);
			var newUrl = res.headers.location;
			newUrl = (newUrl)?newUrl:'';

			var rootUrl = url.match(/http:\/\/[^\/]+/);
			rootUrl = (rootUrl)?rootUrl:'';

			newUrl = (newUrl[0] === '/' && newUrl.length < 120)?rootUrl+newUrl:newUrl; // If it's a local redirect

			if(depth < 5) parent.get(newUrl,callback,depth+1); // bit hackish way of avoiding https

			return;
		} else if(res.statusCode !== 200){
			return console.error('MORE SERIOUS ERROR:'+res.statusCode);
		}

		res.setEncoding('utf8');

		res.on('data', function(d) {
			body.push(d);
		});

		res.on('end', function() {
			console.log("DONE READING\t"+url+"\t"+body.length+" chunks.");
			var result = {};
			result.body = body.join('');
			result.url = url;
			callback(result);

		});

	}).on('error', function(e) {
		console.error(e);
	});	
};

module.exports = fetchUrl;

