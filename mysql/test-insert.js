var mysql      = require('mysql');
var connection = mysql.createConnection({
	host     : 'localhost',
	port	: '8889',
	user     : 'root',
	password : 'root',
	database : 'vine'

});

connection.connect();

// INSERT
console.log('Inserting FOO id');
connection.query("INSERT INTO trends (vine_id, screen_name, count) VALUES ('foo','kaigani','1')", function(err, rows, fields) {
  if (err) throw err; // will throw for duplicate key on vine_id

  console.log('INSERT foo: ', rows);
});

// UPDATE
console.log('Updating FOO id');
connection.query("UPDATE trends SET count='2' WHERE vine_id='foo'", function(err, rows, fields) {
  if (err) throw err; // will throw for duplicate key on vine_id

  console.log('UPDATE foo: ', rows);
});

// UPSERT
console.log('Upserting NEW id');
connection.query("UPDATE trends SET count='2' WHERE vine_id='new'", function(err, rows, fields) {
  if (err) throw err; // will throw for duplicate key on vine_id

  console.log('UPDATE new: ', rows);
  if(rows.affectedRows === 0){
  	console.log('NOT UPDATED do an INSERT');
  	connection.query("INSERT INTO trends (vine_id, screen_name, count) VALUES ('new','kaigani','1')", function(err, rows, fields) {
  		if (err) throw err; // will throw for duplicate key on vine_id

  		console.log('INSERT new: ', rows);
  	});
  }
});


connection.end();