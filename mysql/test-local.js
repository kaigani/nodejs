var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  port	: '8889',
  user     : 'root',
  password : 'root',
  database : 'Company DB'

});

connection.connect();

connection.query('SELECT 1 + 1 AS solution', function(err, rows, fields) {
  if (err) throw err;

  console.log('The solution is: ', rows[0].solution);
});

connection.query('SELECT Industry FROM LinkedIn', function(err, rows, fields) {
  if (err) throw err;

  console.log(rows[0].Industry);
});

connection.end();