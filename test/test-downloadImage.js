var http = require('http') ,
    fs = require('fs') ,
    options;

options = {
    host: 'a0.twimg.com' ,
    port: 80 ,
    path: '/profile_images/3075898644/192103695b61f27de008064ea92df0a2_normal.jpeg'
};

var request = http.get(options, function(res){
    var imagedata = '';
    res.setEncoding('binary');

    res.on('data', function(chunk){
        imagedata += chunk;
    });

    res.on('end', function(){
        fs.writeFile('avatar.jpeg', imagedata, 'binary', function(err){
            if (err) throw err;
            console.log('File saved.');
        });
    });

});