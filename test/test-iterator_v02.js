
// Do something 10 times then have a cooldown before doing it again
// For a set of 100

// GLOBALS
var g_startFrom = 0; // starting point
var g_nIterations = 10;
var g_intervalSpeed = 1000; // ms per loop
var g_cooldown = 10000; // in ms
var g_setLength = 100; // how many items to iterate through
var g_intervalID;


// BEGIN 
console.log("Starting test");

g_intervalID = setInterval(iterate,g_intervalSpeed,fn);

//
// ITERATOR
//
// set globals - g_startFrom, g_nIterations, g_cooldown, g_setLength, g_intervalID

// ITERATION FUNCTION
function fn(index){

	// TEST STOP CONDITION
	if(index < g_setLength){
		console.log("I did something at index="+index);
		return true;
	}
	return false;
}

// ITERATOR
function iterate(fn){
	this.limit = g_nIterations;
	this.count = (this.count)?this.count:0;
	this.index = (this.index)?this.index:g_startFrom;

	if(this.count < this.limit){

		// DO ITERATION FN
		if( fn(this.index) ){
			// SUCCESS - continue iterating
			console.log("Iterated index="+this.index+", count="+this.count);
			this.count++;
			this.index++;
		}else{
			// FAIL - stop condition
			clearInterval(g_intervalID);
		}
		
	}else{
		// set cooldown period
		console.log("Cooldown --- "+g_cooldown+"ms");
		clearInterval(g_intervalID);
		this.count = 0;
		g_startFrom = this.index;
		setTimeout(continueIterate,g_cooldown); // 900 000 is 15 minutes
	}
}

function continueIterate() {
	g_intervalID = setInterval(iterate,g_intervalSpeed,fn);

	// RESET ANY RELATED GLOBALS ...

}