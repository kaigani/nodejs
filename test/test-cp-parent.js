var cp = require('child_process');

var n = cp.fork('test-cp-child-error.js');

n.on('message', function(m) {
  console.log('PARENT got message:', m);
});

n.on('exit', function() {
	console.log('PARENT got EXIT event from child');
	console.log('PARENT exiting');
	process.exit();
});

n.on('error', function(err) {
	console.log('PARENT got ERROR event from child');
	n.kill();
	n = cp.fork('test-cp-child.js');
	n.send({ hello: 'world' });
});

n.send({ hello: 'world' });


