//
// requiredTest.js - sample require module with a single function, test()
//
// Based on: http://caolanmcmahon.com/posts/writing_for_node_and_the_browser/
//

(function(exports){

	exports.test = function(){
        return 'hello world';
    };

})(typeof exports === 'undefined'? this['GenericModule']={}: exports);