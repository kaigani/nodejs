//
// fetchUrl - is a custom http/https handler that manages redirects
//

var fetchUrl = require('fetchUrl.js');
var fs = require('fs');

var args = process.argv.slice(2);

var url = (args[0])?args[0]:'http://bit.ly/11z49ZC';
// test
fetchUrl.get(url, function(result){
	console.log("Retrieved url ...");
	console.log(result.url);
	fs.writeFile('fetchdata.txt', result.body, function(err) {
    if (err) throw err;
    console.log('file saved');
  	});
});

