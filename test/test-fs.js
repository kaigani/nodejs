
var fs = require('fs');

fs.readFile('fs_readfile.txt', 'utf8', function (err, data) {
  if (err) throw err;

  var lines = data.split('\n');
  console.log(lines);
  
});

var data = "this is the file being written out";

fs.writeFile('fs_writefile.txt', data, function(err) {
    if (err) throw err;
    console.log('file saved');
});
