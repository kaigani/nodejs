
console.log("Starting test");

var intervalID = setInterval(callback,1000,function(){
	// this is an internal function for data management between callbacks
	this.count = (this.count)?this.count:0;

	console.log("Count is set to:"+this.count);
	this.count++;
	return(this.count);
});

function callback(counter){
	this.mylocal = (this.mylocal)?this.mylocal:0;

	var count = counter();
	console.log("Found count="+count+", my local="+this.mylocal);
	this.mylocal++;
	if(count > 20) clearInterval(intervalID);
}