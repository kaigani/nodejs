
// Do something 100 times then have a cooldown before doing it again
// For a set of 1000

console.log("Starting test");

var intervalID = setInterval(iterate,1,fn);

function fn(index){
	console.log("I did something at index="+index);
}

function iterate(fn,fromIndex){
	this.limit = 10;
	this.count = (this.count)?this.count:0;
	this.index = (this.count === 0)?fromIndex:this.index;
	this.index = (this.index)?this.index:0;

	if(this.count < this.limit){
		// do something
		fn(this.index);
		console.log("Iterate index="+this.index+", count="+this.count);
		this.count++;
		this.index++;
		if(this.index >= 100) clearInterval(intervalID);
	}else{
		// set cooldown period
		console.log("Cooldown 5s");
		clearInterval(intervalID);
		this.count = 0;
		setTimeout(continueIterate,1000,this.index); // 900 000 is 15 minutes
	}
}

function continueIterate(fromIndex) {
	intervalID = setInterval(iterate,1,fn,fromIndex);
}