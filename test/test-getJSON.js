var http = require('http');

var url = "http://en.wikipedia.org/w/api.php?action=query&list=allimages&ailimit=100&aiminsize=32000&format=json&aifrom=hello";

var request = http.get(url, function(res){
    var body = '';
    //res.setEncoding('binary');

    res.on('data', function(chunk){
        body += chunk;
    });

    res.on('end', function(){
        var obj = JSON.parse(body);
        console.log("Got response: ");
        for(var item in obj){
            console.log('\t',item);
        }
    });

}).on('error', function(e) {
      console.log("Got error: ", e);
});