var io = require('socket.io').listen(1973);
var count = 0;

io.sockets.on('connection', function (socket) {
	socket.on('message', function (msg) {
		console.log("\nGOT A MESSAGE!\t"+msg+"\n");
	});
	socket.on('disconnect', function () {
		console.log("\nDISCONNECT\n");
	});
	socket.emit('news', 'News of the world');
	socket.emit('message', 'A wonderful and exciting message');
});

setInterval(function(){
	count++;
	io.sockets.emit('message', 'Public announcement number '+count);
},5000);
