console.log("Starting log test...");

var mongoose = require('mongoose');
mongoose.connect('localhost', 'test');

var logSchema = {
	tweet_id: 'string',
	timestamp: 'string',
	user_id: 'string',
	screen_name: 'string',
	verified: 'boolean',
	vine_id: 'string',
	isRetweet: 'boolean',
	source: 'string'
};

var schema = mongoose.Schema(logSchema);
var tweetStub = mongoose.model('tweetStub', schema);

var tweetObj = {};
tweetObj.tweet_id = '00001';
tweetObj.timestamp = '12 Mar 2004';
tweetObj.user_id = '4000000';
tweetObj.screen_name = 'kaigani';

var doc = new tweetStub(tweetObj);

doc.save(function (err) {
  if (err) // ...
  console.log('ERROR SAVING TWEET');
});

console.log("Primary thread done.");